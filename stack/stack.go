// stack contains an implementation of a thread-safe and blocking stack.

package stack

import (
	"log"
	"sync"
)

// Stack is thread-safe and blocks if Pop is attempted on an empty stack or
// Push is attempted on a full stack.
type Stack struct {
	mu        sync.Mutex
	elements  []interface{}
	blocker   chan struct{}
	maxLength int
}

// NewStack initialises and returns an instance of Stack with a maximum length of maxLength.
func NewStack(maxLength int) *Stack {
	stack := new(Stack)
	stack.maxLength = maxLength
	stack.blocker = make(chan struct{})
	stack.elements = make([]interface{}, 0, maxLength)
	return stack
}

// Push pushes an element onto the stack. If the stack is full, this function will
// block until a Pop operation frees up space on the stack.
func (s *Stack) Push(element interface{}) {
	s.mu.Lock()
	defer s.mu.Unlock()
	defer log.Println("Pushed", element)

	if len(s.elements) >= s.maxLength {
		// Unlock the mutex while we wait for a pop operation
		s.mu.Unlock()

		s.blocker <- struct{}{}

		// Lock the mutex again. It will get unlocked by the defer statement.
		s.mu.Lock()
	}

	s.elements = append(s.elements, element)

	// Unblock a blocked pop operation.
	select {
	case s.blocker <- struct{}{}:
	default:
	}
}

// Pop removes an element from the stack and returns it. If the stack is empty,
// this function will block until a Push operation adds an element to the stack.
func (s *Stack) Pop() interface{} {
	s.mu.Lock()
	defer s.mu.Unlock()

	var element interface{}
	defer func() {
		log.Println("popped", element)
	}()

	if len(s.elements) == 0 {
		// Unlock the mutex while we wait for a push operation
		s.mu.Unlock()

		<-s.blocker

		// Lock the mutex again. It will get unlocked by the defer statement.
		s.mu.Lock()
	}

	// The last element is the top of the stack.
	element = s.elements[len(s.elements)-1]

	// Remove the last element from the array.
	s.elements = s.elements[:len(s.elements)-1]

	// Unblock a blocked push operation.
	select {
	case <-s.blocker:
	default:
	}

	return element
}
