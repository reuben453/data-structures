package stack

import (
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestOperations(t *testing.T) {
	stack := NewStack(2)

	stack.Push("A")
	stack.Push("B")

	elem := stack.Pop()
	assert.Equal(t, "B", elem)

	elem = stack.Pop()
	assert.Equal(t, "A", elem)

	assert.Equal(t, 0, len(stack.elements))

	stack.Push("C")
	elem = stack.Pop()
	assert.Equal(t, "C", elem)

	assert.Equal(t, stack.maxLength, cap(stack.elements))

}

func TestParallelOPerations(t *testing.T) {

	stack := NewStack(2)

	assert.Equal(t, stack.maxLength, cap(stack.elements))

	wg := new(sync.WaitGroup)

	wg.Add(1)
	go func() {
		defer wg.Done()
		for i := 0; i < 20; i++ {
			stack.Pop()
			// fmt.Println("popped", elem)
		}
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		for i := 0; i < 20; i++ {
			stack.Push(i + 1)
			// fmt.Println("pushed", i+1)
		}
	}()

	wg.Wait()

	assert.Equal(t, 0, len(stack.elements))

	assert.Equal(t, stack.maxLength, cap(stack.elements))
}
