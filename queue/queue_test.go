package queue

import (
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestOperations(t *testing.T) {
	q := NewQueue(2)

	q.Push(1)
	q.Push(2)

	elem := q.Pop()
	assert.Equal(t, 1, elem)

	elem = q.Pop()
	assert.Equal(t, 2, elem)
}

func TestParallelOPerations(t *testing.T) {

	q := NewQueue(2)

	wg := new(sync.WaitGroup)

	wg.Add(1)
	go func() {
		defer wg.Done()
		for i := 0; i < 20; i++ {
			q.Pop()
			// fmt.Println("popped", elem)
		}
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		for i := 0; i < 20; i++ {
			q.Push(i + 1)
			// fmt.Println("pushed", i+1)
		}
	}()

	wg.Wait()

	assert.Equal(t, 0, len(q.elements))
}
