// queue contains an implementation of a thread-safe and blocking queue.

package queue

import (
	"log"
)

// Queue is thread-safe and blocks if Pop is attempted on an empty queue or
// Push is attempted on a full queue.
type Queue struct {
	elements chan interface{}
}

// NewQueue initialises and returns an instance of Queue with a maximum length of maxLength.
func NewQueue(maxLength int) *Queue {
	q := Queue{}
	q.elements = make(chan interface{}, maxLength)
	return &q
}

// Push pushes an element onto the queue. If the queue is full, this function will
// block until a Pop operation frees up space on the queue.
func (q *Queue) Push(element interface{}) {
	q.elements <- element

	// The log statement is not guaranteed to print before any other operation is performed on the queue.
	log.Println("Pushed", element)
}

// Pop removes an element from the queue and returns it. If the queue is empty,
// this function will block until a Push operation adds an element to the queue.
func (q *Queue) Pop() interface{} {
	elem := <-q.elements

	// The log statement is not guaranteed to print before any other operation is performed on the queue.
	log.Println("Popped", elem)

	return elem
}
