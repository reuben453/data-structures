# Queue

This is a thread-safe implementation of a queue that blocks on a Push operation to a full queue and a Pop operation from an empty queue.

This is implemented using a channel. Since a channel behaves exactly like a queue, is thread-safe, and blocks if you try reading from an empty channel or if you try pushing data to a full channel, it is a blocking and concurrent queue.

This queue provides two operations: Push and Pop.


## Test
`go test -v -race -cover -timeout 30s gitlab.com/reuben453/data-structures/queue`

The test runs Push and Pop operations simultaneously in separate goroutines to demonstrate that there is no race.

# Stack

This is a thread-safe implementation of a stack that blocks on a Push operation to a full stack and a Pop operation from an empty stack.

This implementation uses an array to hold the elements of the stack, a mutex to synchronise access to the array, and a non-buffered channel to block on a Push to a full stack or a Pop from an empty stack.

## Test
`go test -v -race -cover -timeout 30s gitlab.com/reuben453/data-structures/stack`

The test runs Push and Pop operations simultaneously in separate goroutines to demonstrate that there is no race.

This stack provides two operations: Push and Pop.